;; ==========================================================
;; TODO: This is unfinished... revisit

;; 2520 is the smallest number that can be divided by each of the numbers
;; from 1 to 10 without any remainder.

;; What is the smallest positive number that is evenly divisible by all of the
;; numbers from 1 to 20?


;; (* 1 2 3   4    5   6   7    8      9    10)
;; (*   2 3  2 2   5  2 3  7  2 2 2   3 3  5 2) ;; prime factorization
;; (*                      7  2 2 2   3 3  5)
(def primes [2 3 5 7 11 13 17 19])

(defn prime? [n]
  (some #(= n %) primes))

(defn divides? [num denom]
  (zero? (rem num denom)))

(defn factors
  ([n] (factors n []))
  ([n v]
   (if (prime? n)
     (conj v n)
     (let [factor (first (filter #(divides? n %) primes))]
       (recur (/ n factor) (conj v factor))))))



(def nums (range 2 (inc 10)))
(map factors nums)


