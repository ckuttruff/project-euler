(ns primes)

;; Using Sieve of Eratosthenes; certainly not the most efficient,
;;   but will generate 1M primes in less than 10s

(defn primes
  ([max-num] (primes max-num 0 (range 2 (inc max-num))))
  ([max-num idx nums]
   (if (> (inc idx) (Math/sqrt (count nums)))
     nums
     (let [curr-num (nth nums idx)
           not-multiple? (fn [n] (or (= n curr-num)
                                     (not= 0 (rem n curr-num))))]
       (recur max-num (inc idx)
              (filter not-multiple? nums))))))
(def primes (memoize primes))

(defn prime? [n]
  (let [min-denom 2
        max-denom (Math/sqrt n)]
    (loop [curr min-denom]
      (cond (< n 2) false
            (> curr max-denom) true
            (zero? (rem n curr)) false
            :else (recur (inc curr))))))
