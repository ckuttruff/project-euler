;; https://projecteuler.net/problem=11

;; a < b < c, a^2 + b^2 = c^2, a + b + c = 1000

;; Key mathematical property (from Wikipedia):
;;   If (a, b, c) is a Pythagorean triple, so is (ka, kb, kc) for any positive integer k

(def result-sum 1000)
(def base-triples [[3 4 5]    [5 12 13]  [8 15 17]  [7 24 25]
                   [20 21 29] [12 35 37] [9 40 41]  [28 45 53]
                   [11 60 61] [16 63 65] [33 56 65] [48 55 73]
                   [13 84 85] [36 77 85] [39 80 89] [65 72 97]])

(defn scale-triple [v k]
  (map #(* % k) v))

(defn triples
  ([] (triples 1))
  ;; k is constant factor to apply to
  ([k]
   (let [coll (map #(scale-triple % k) base-triples)]
     (lazy-seq
      (concat coll (triples (inc k)))))))

(defn triple-result? [v]
  (let [[a b c] v
        sum (+ a b c)]
    (= sum result-sum)))

(let [res (filter triple-result? (take 1000 (triples)))
      coll (first res)]
  (apply * coll))
