;; https://projecteuler.net/problem=1

(defn sum-multiples
  "Sums all multiples of m1 or m2 in vector v"
  [v m1 m2]
  (letfn [(divides [n] (or (zero? (mod n m1))
                           (zero? (mod n m2))))]
    (apply + (filter #(divides %) v))))

(sum-multiples (range 1 1000) 3 5)

