;; https://projecteuler.net/problem=2

(defn sum-even-fibs
  ([max] (sum-even-fibs max 0 1 0))
  ([max n-2 n-1 result]
   (let [new-fib (+' n-2 n-1)
         new-result (if (even? new-fib)
                      (+' new-fib result)
                      result)]
     (if (> new-fib max)
       result
       (recur max n-1 new-fib new-result)))))

(sum-even-fibs 4000000N)


