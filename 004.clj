;; https://projecteuler.net/problem=4

(def max-num 999)
(def min-num 900)

(defn palindrome? [n]
  (= (str n)
     (apply str (reverse (str n)))))

(let [num-range (range max-num min-num -1)
      nums (for [a num-range
                 b num-range] (* a b))]
  (first (sort > (filter palindrome? nums))))

