;; https://projecteuler.net/problem=6

(def nums (range 1 101))

(defn sum-squares [nums]
  (apply + (map #(* % %) nums)))

(defn square-sums [nums]
  (let [n (apply + nums)]
    (* n n)))

(- (square-sums nums) (sum-squares nums))
