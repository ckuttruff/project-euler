;; https://projecteuler.net/problem=7

(load-file "primes.clj")

;; 10,001th prime
(nth (primes/primes 1000000) 10000)
