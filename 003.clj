;; https://projecteuler.net/problem=3

(def num 600851475143)

(defn divides [m n]
  (zero? (mod m n)))

(defn get-factors
  ([n] (get-factors n #{} 2))
  ([n factors curr-factor]

   (if (>= curr-factor (Math/sqrt num))
     factors
     (if (divides n curr-factor)
       (recur (/ n curr-factor) (conj factors curr-factor) curr-factor)
       (recur n factors (inc curr-factor))))))

(defn prime? [n]
  (and (> n 1)
       (= (get-factors n) #{n})))

(apply max (filter prime? (get-factors num)))


