;; https://projecteuler.net/problem=10

(load-file "primes.clj")

(apply + (primes/primes 2000000))
